# Frontend de Gestão de Contratos

Este é um projeto simples para o frontend do Gestão de Contratos, ele foi escrito com ReactJs. É um CRUD simples para contratos e partes envolvidas. Bibliotecas adicionais utilizadas para auxiliar o desenvolvimento:

- [React Router Dom](https://www.npmjs.com/package/react-router-dom): para gerenciar navegação
- [React Bootstrap](https://www.npmjs.com/package/react-bootstrap): para auxiliar no layout/desing da aplicação
- [React Icons](https://www.npmjs.com/package/react-icons): para ícones
- [Axios](https://www.npmjs.com/package/axios): para chamadas a API do backend
- [Formik](https://www.npmjs.com/package/formik): para gerenciar formulários
- [Yup](https://www.npmjs.com/package/yup): para validações nos formulários

# Estrutura dos códigos

Toda a aplicação usa um compartilhamento global de estado, através do uso de uma Store, Context e Reducer. Em relação a componentes, temos o setup inicial da API dentro da pasta "services". Telas "inteiras" dentro da pasta "screens" e componentes reutilizáveis na pasta "components".

# Instalação

Para instalar todas as dependências basta executar:

```sh
npm install
```

# Setup do Backend

Para possibilitar a execução da aplicação é preciso ter o backend em execução. Todos o setup do backend está descrito no projeto do [Backend](https://gitlab.com/eduardo_kandalski/contratos-backend).

Atente que o Backend executa na porta 4000 e o frontend na porta 3000.

# Iniciar o Frontend

Para iniciar a aplicação, no momento em modo de desenvolvimento, executar o comando:

```sh
npm start
```

# Bug conhecidos

- Após login, o token tem validade de 1 hora, mas não existe uma validação da expiração do token, todas as chamadas ao backend vão falhar por erro na autenticação.
- É possível deletar uma parte mesmo que ela esteja ligada a um contrato, isso gerará uma falha ao carregar os dados do contrato.
- A deleção de um contrato, deleta o registro do Banco MongoDB mas o arquivo de upload não é deletado do backend propriamente (bug do backend)
- Ao escolher arquivo para upload, é informado utilizado um filtro para arquivos .PDF, .DOC e .DOCX mas nada impede o usuário de alterar o filtro e inserir outro tipo de arquivo. Falta a validação adicional para a extensão/tipo do arquivo.
- Na tela de um contrato é possível informar datas erradas, como 30/02/2020. A regex utilizada trata um formato "básico" de datas.
- Os erros retornados pelo backend são devidamente mostrados através de mensagens de erro somente nas partes de Criação de novo usuário e login. Os erros do CRUD propriamente não foram tratados até o momento.

# Melhorias a serem adicionadas

- Adicionar a verificação da validade do token (JWT) e solicitar novo login após o mesmo expirar.
- Adicionar validação antes de deletar uma parte, caso esteja vinculada a ao menos um contrato, não permitir a exclusão.
- Adicionar validação adicional para a extensão/tipo do arquivo de upload, para permitir somente MSWord e PDF.
- Melhorias na tela de listagem de partes:
  - Adicionar filtros para busca e ordenação nas colunas
- Melhorias na tela de cadastro/edição de uma parte:
  - Máscara para o CPF
  - Máscara para o telefone
  - Solicitação de confirmação antes de deletar uma parte
- Melhorias na tela de listagem de contratos:
  - Adicionar filtros para busca e ordenação nas colunas
- Melhorias na tela de cadastro/edição de um contrato:
  - Campos de data (Início e Fim) serem alterados para um DatePicker e trabalhar com o tipo Date ao invés de string
  - Solicitação de confirmação antes de deletar um contrato
  - Adicionar filtros para facilitar busca de uma parte tanto nas partes adicionadas quanto na tabela de todas as partes. Com muitas partes adicionadas fica muita informação na tela.
  - Possibilitar trocar o arquivo na edição do contrato. Hoje somente pode anexar o arquivo na criação do contrato. Após a criação não é mais possível ajsutar.
- Melhorias gerais no layout/design da aplicação
- Melhorias gerais para gerar mais componentes reutilizáveis, tais como tabelas.

# Contribuição/Contato

Você pode me encontrar no Twitter [@edukandalski](https://twitter.com/edukandalski).
