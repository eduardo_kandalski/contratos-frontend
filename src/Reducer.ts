const Reducer = (state, action) => {
  switch (action.type) {
    case 'LOGIN': {
      return {
        ...state,
        user: action.payload.user,
      };
    }
    case 'LOGOUT': {
      return {
        ...state,
        user: undefined,
      };
    }
    case 'SET_CONTRACTS': {
      return {
        ...state,
        contracts: action.payload.contracts,
      };
    }
    case 'ADD_CONTRACT': {
      let contracts = [...state.contracts];
      contracts.push(action.payload.contract);
      return {
        ...state,
        contracts,
      };
    }
    case 'EDIT_CONTRACT': {
      let contracts = [...state.contracts];
      const index = contracts.findIndex((c) => c._id === action.payload.contract._id);
      contracts[index] = action.payload.party;
      return {
        ...state,
        contracts,
      };
    }
    case 'DELETE_CONTRACT': {
      const contracts = [...state.contracts];
      const updatedContracts = contracts.filter((c) => c._id !== action.payload.id);
      return {
        ...state,
        contracts: updatedContracts,
      };
    }
    case 'SET_PARTIES': {
      return {
        ...state,
        parties: action.payload.parties,
      };
    }
    case 'ADD_PARTY': {
      let parties = [...state.parties];
      parties.push(action.payload.party);
      return {
        ...state,
        parties,
      };
    }
    case 'EDIT_PARTY': {
      let parties = [...state.parties];
      const index = parties.findIndex((p) => p._id === action.payload.party._id);
      parties[index] = action.payload.party;
      return {
        ...state,
        parties,
      };
    }
    case 'DELETE_PARTY': {
      const parties = [...state.parties];
      const updatedParties = parties.filter((p) => p._id !== action.payload.id);
      return {
        ...state,
        parties: updatedParties,
      };
    }
    default:
      return state;
  }
};

export default Reducer;
