import React, { useContext } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

import { Context } from './Store';
import Home from './Home';
import Login from './screens/Login';
import Signup from './screens/Signup';
import Logout from './screens/Logout';
import Navigbar from './components/Navigbar';
import Contracts from './screens/Contracts';
import Parties from './screens/Parties';
import DetailParty from './screens/Parties/DetailParty';
import DetailContract from './screens/Contracts/DetailContract';
import NotFound from './screens/NotFound';

function App() {
  const [state, dispatch] = useContext(Context);

  return (
    <>
      {state.user && <Navigbar />}
      <Switch>
        <Route exact path="/" render={() => <Redirect to="/home" />} />
        <Route path="/home" exact component={Home} />
        <Route path="/login" exact component={Login} />
        <Route path="/signup" exact component={Signup} />
        <Route path="/logout" exact component={Logout} />
        <Route path="/contracts/:id" exact component={DetailContract} />
        <Route path="/contracts" exact component={Contracts} />
        <Route path="/parties/:id" exact component={DetailParty} />
        <Route path="/parties" exact component={Parties} />
        <Route path="/notfound" exact component={NotFound} />
        <Redirect to="/notfound" />
      </Switch>
    </>
  );
}

export default App;
