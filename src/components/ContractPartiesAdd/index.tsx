import React, { useContext, useState } from 'react';
import { Table } from 'react-bootstrap';
import { IconContext } from 'react-icons';
import { FiPlus } from 'react-icons/fi';

import { Context } from '../../Store';

const ContractPartiesAdd = ({ addedParties, addParty, upperState }) => {
  const [state, dispatch] = useContext(Context);
  const displayData = state.parties.filter((el) => addedParties.findIndex((ad) => ad === el._id) === -1);

  return (
    <>
      <p>Adicionar partes:</p>
      <Table striped bordered hover>
        <thead>
          <tr className="text-center">
            <th>Nome</th>
            <th>Sobrenome</th>
            <th>E-mail</th>
            <th>CPF</th>
            <th>Telefone</th>
            <th>Adicionar</th>
          </tr>
        </thead>
        <tbody>
          {displayData.map((party) => (
            <tr key={party._id} className="text-center">
              <td>{party.name}</td>
              <td>{party.surname}</td>
              <td>{party.email}</td>
              <td>{party.cpf}</td>
              <td>{party.phone}</td>
              <td>
                <IconContext.Provider value={{ size: '1.5em', color: 'seagreen', className: 'icon-cont' }}>
                  <div>
                    <FiPlus onClick={() => addParty(party._id, upperState)} />
                  </div>
                </IconContext.Provider>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </>
  );
};

export default ContractPartiesAdd;
