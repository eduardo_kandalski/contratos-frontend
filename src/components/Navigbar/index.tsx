import React, { useContext } from 'react';
import { Link } from 'react-router-dom';
import { Nav, Navbar } from 'react-bootstrap';

import { Context } from '../../Store';

const Navigbar = () => {
  const [state, dispatch] = useContext(Context);

  return (
    <>
      <Navbar bg="light" expand="lg">
        <Navbar.Brand as={Link} to="/">
          Gestão
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
            <Nav.Link as={Link} to="/contracts">
              Contratos
            </Nav.Link>
            <Nav.Link as={Link} to="/parties">
              Partes
            </Nav.Link>
          </Nav>
        </Navbar.Collapse>
        <Navbar.Collapse className="justify-content-end">
          <Navbar.Text>
            {state.user.name} {state.user.surname}
          </Navbar.Text>
          <Nav.Link as={Link} to="/logout">
            Sair
          </Nav.Link>
        </Navbar.Collapse>
      </Navbar>
    </>
  );
};

export default Navigbar;
