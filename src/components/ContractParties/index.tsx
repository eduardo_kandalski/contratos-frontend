import React, { useContext } from 'react';
import { Table } from 'react-bootstrap';
import { IconContext } from 'react-icons';
import { FiTrash2 } from 'react-icons/fi';

import { Context } from '../../Store';

const ContractParties = ({ parties, removeParty, mode, upperState }) => {
  const [state, dispatch] = useContext(Context);

  const addedParties = state.parties.filter((p) => parties.findIndex((pt) => pt === p._id) >= 0);

  return (
    <>
      {parties.length === 0 && <p>Nenhuma parte relacionada!</p>}
      {parties.length > 0 && (
        <>
          <p>Partes relacionadas:</p>
          <Table striped bordered hover>
            <thead>
              <tr className="text-center">
                <th>Nome</th>
                <th>Sobrenome</th>
                <th>E-mail</th>
                <th>CPF</th>
                <th>Telefone</th>
                {mode !== 'show' && <th>Remover</th>}
              </tr>
            </thead>
            <tbody>
              {addedParties.map((party) => (
                <tr key={party._id} className="text-center">
                  <td>{party.name}</td>
                  <td>{party.surname}</td>
                  <td>{party.email}</td>
                  <td>{party.cpf}</td>
                  <td>{party.phone}</td>
                  {mode !== 'show' && (
                    <td>
                      <IconContext.Provider value={{ size: '1.5em', color: 'crimson', className: 'icon-cont' }}>
                        <div>
                          <FiTrash2 onClick={() => removeParty(party._id, upperState)} />
                        </div>
                      </IconContext.Provider>
                    </td>
                  )}
                </tr>
              ))}
            </tbody>
          </Table>
        </>
      )}
    </>
  );
};

export default ContractParties;
