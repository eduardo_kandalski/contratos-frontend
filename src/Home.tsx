import React, { useContext } from 'react';
import { Redirect } from 'react-router-dom';
import { Container } from 'react-bootstrap';

import { Context } from './Store';

const Home = () => {
  const [state, dispatch] = useContext(Context);

  if (!state.user) {
    return <Redirect to="/login" />;
  }

  return (
    <Container className="frame">
      <h3>
        Bem vindo: {state.user.name} {state.user.surname}.
      </h3>
      <p>Você pode gerenciar contratos e partes a partir da barra de navegação.</p>
    </Container>
  );
};

export default Home;
