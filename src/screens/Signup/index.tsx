import React, { useContext, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { Formik } from 'formik';
import * as Yup from 'yup';

import { Container, Form, Button, FormControl, Alert } from 'react-bootstrap';
import { Context } from '../../Store';
import Api from '../../services/api';

const validationSchema = Yup.object().shape({
  email: Yup.string().email('Inserir um e-mail válido').required('E-mail é obrigatório'),
  name: Yup.string().min(2, 'Nome precisa contar ao menos 2 caracteres').required('Nome é obrigatório'),
  surname: Yup.string().min(2, 'Nome precisa contar ao menos 2 caracteres').required('Nome é obrigatório'),
  password: Yup.string().min(8, 'Senha precisa ter ao menos 8 caracteres').required('Senha é obrigatório'),
  repeatedPassword: Yup.string().min(8, 'Senha precisa ter ao menos 8 caracteres').required('Senha é obrigatório'),
});

const Signup = () => {
  const [state, dispatch] = useContext(Context);
  const history = useHistory();
  const [errorMsg, setErrorMsg] = useState('');

  const handleSubmit = async (values, { setSubmitting, resetForm }) => {
    const { email, name, surname, password, repeatedPassword } = values;
    setSubmitting(true);

    if (password !== repeatedPassword) {
      setErrorMsg('As senhas não são iguais');
      setSubmitting(false);
      return;
    }

    try {
      const res = await Api.post('/signup', { email, password, name, surname });
      const user = { email: res.data.email, name: res.data.name, surname: res.data.surname, token: res.data.token };
      localStorage.setItem('user', JSON.stringify(user));
      dispatch({ type: 'LOGIN', payload: { user } });
      history.replace('/home');
    } catch (err) {
      setErrorMsg('Ocorreu um erro ao tentar criar o usuário. Tente novamente mais tarde.');
    }

    setSubmitting(false);
  };

  return (
    <>
      <Container className="frame">
        <h2>Crie sua conta</h2>
        <p>Favor preencha todos os campos abaixo</p>

        <Formik
          initialValues={{ email: '', name: '', surname: '', password: '', repeatedPassword: '' }}
          validationSchema={validationSchema}
          onSubmit={handleSubmit}
        >
          {({ values, errors, touched, handleChange, handleBlur, handleSubmit, isSubmitting }) => (
            <Form noValidate onSubmit={handleSubmit}>
              <Form.Group controlId="formBasicEmail">
                <Form.Label>E-mail:</Form.Label>
                <Form.Control
                  type="email"
                  placeholder="Seu e-mail"
                  name="email"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.email}
                  isInvalid={touched.email && errors.email}
                />
                {touched.email && errors.email ? <FormControl.Feedback type="invalid">{errors.email}</FormControl.Feedback> : null}
              </Form.Group>
              <Form.Group controlId="formBasicName">
                <Form.Label>Nome:</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Seu nome"
                  name="name"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.name}
                  isInvalid={touched.name && errors.name}
                />
                {touched.name && errors.name ? <FormControl.Feedback type="invalid">{errors.name}</FormControl.Feedback> : null}
              </Form.Group>
              <Form.Group controlId="formBasicSurname">
                <Form.Label>Sobrenome:</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Seu sobrenome"
                  name="surname"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.surname}
                  isInvalid={touched.surname && errors.surname}
                />
                {touched.surname && errors.surname ? <FormControl.Feedback type="invalid">{errors.surname}</FormControl.Feedback> : null}
              </Form.Group>
              <Form.Group controlId="formBasicPassword">
                <Form.Label>Senha:</Form.Label>
                <Form.Control
                  type="password"
                  placeholder="Sua senha"
                  name="password"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.password}
                  isInvalid={touched.password && errors.password}
                />
                {touched.password && errors.password ? <FormControl.Feedback type="invalid">{errors.password}</FormControl.Feedback> : null}
              </Form.Group>
              <Form.Group controlId="formBasicRepeatedPassword">
                <Form.Label>Repita a senha:</Form.Label>
                <Form.Control
                  type="password"
                  placeholder="Sua senha novamente"
                  name="repeatedPassword"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.repeatedPassword}
                  isInvalid={touched.repeatedPassword && errors.repeatedPassword}
                />
                {touched.repeatedPassword && errors.repeatedPassword ? (
                  <FormControl.Feedback type="invalid">{errors.repeatedPassword}</FormControl.Feedback>
                ) : null}
              </Form.Group>
              <Button variant="outline-primary" type="submit" disabled={isSubmitting}>
                Enviar
              </Button>
            </Form>
          )}
        </Formik>
        <br />
        {errorMsg && <Alert variant="danger">{errorMsg}</Alert>}
      </Container>
    </>
  );
};

export default Signup;
