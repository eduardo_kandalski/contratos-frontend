import React from 'react';
import { Link } from 'react-router-dom';
import { Container } from 'react-bootstrap';

const NotFound = () => {
  return (
    <Container className="frame">
      <h3>Recurso não encontrado.</h3>
      <Link to="/home">Retornar para home</Link>
    </Container>
  );
};

export default NotFound;
