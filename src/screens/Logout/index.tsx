import React, { useContext, useEffect } from 'react';
import { Redirect } from 'react-router-dom';

import { Context } from '../../Store';

const Logout = () => {
  const [state, dispatch] = useContext(Context);

  useEffect(() => {
    localStorage.removeItem('user');
    dispatch({ type: 'LOGOUT' });
  }, []);

  return <Redirect to="/home" />;
};

export default Logout;
