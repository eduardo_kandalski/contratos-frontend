import React, { useContext, useState } from 'react';
import { useHistory, Link } from 'react-router-dom';
import { Formik } from 'formik';
import * as Yup from 'yup';

import { Container, Form, Button, FormControl, Alert } from 'react-bootstrap';
import { Context } from '../../Store';
import Api from '../../services/api';

const validationSchema = Yup.object().shape({
  email: Yup.string().email('Inserir um e-mail válido').required('E-mail é obrigatório'),
  password: Yup.string().required('Senha é obrigatório'),
});

const Login = () => {
  const [state, dispatch] = useContext(Context);
  const history = useHistory();
  const [errorMsg, setErrorMsg] = useState('');

  const handleSubmit = async (values, { setSubmitting, resetForm }) => {
    const { email, password } = values;
    setSubmitting(true);

    try {
      const res = await Api.post('/signin', { email, password });
      const user = { email: res.data.email, name: res.data.name, surname: res.data.surname, token: res.data.token };
      localStorage.setItem('user', JSON.stringify(user));
      dispatch({ type: 'LOGIN', payload: { user } });
      history.replace('/home');
    } catch (err) {
      setErrorMsg(err.response.data.error);
    }

    setSubmitting(false);
  };

  return (
    <>
      <Container className="frame">
        <h2>Bem vindo ao Gestão de Contratos</h2>

        <p>Favor realize seu login</p>

        <Formik initialValues={{ email: '', password: '' }} validationSchema={validationSchema} onSubmit={handleSubmit}>
          {({ values, errors, touched, handleChange, handleBlur, handleSubmit, isSubmitting }) => (
            <Form onSubmit={handleSubmit}>
              <Form.Group controlId="formBasicEmail">
                <Form.Label>E-mail:</Form.Label>
                <Form.Control
                  type="email"
                  placeholder="Seu e-mail"
                  name="email"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.email}
                  isInvalid={touched.email && errors.email}
                />
                {touched.email && errors.email ? <FormControl.Feedback type="invalid">{errors.email}</FormControl.Feedback> : null}
              </Form.Group>
              <Form.Group controlId="formBasicPassword">
                <Form.Label>Senha:</Form.Label>
                <Form.Control
                  type="password"
                  placeholder="Sua senha"
                  name="password"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.password}
                  isInvalid={touched.password && errors.password}
                />
                {touched.password && errors.password ? <FormControl.Feedback type="invalid">{errors.password}</FormControl.Feedback> : null}
              </Form.Group>
              <Button variant="outline-primary" type="submit" disabled={isSubmitting}>
                Login
              </Button>
            </Form>
          )}
        </Formik>
        <br />
        <p>
          Não é registrado ainda? Crie seu login{' '}
          <span>
            <Link to="/signup">aqui</Link>
          </span>
          .
        </p>
        {errorMsg && <Alert variant="danger">{errorMsg}</Alert>}
      </Container>
    </>
  );
};

export default Login;
