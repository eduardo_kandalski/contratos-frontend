import React, { useContext, useEffect, useState } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import { Container, Form, Button, FormControl, Alert } from 'react-bootstrap';
import { Formik } from 'formik';
import * as Yup from 'yup';
import Api from '../../services/api';

import { Context } from '../../Store';

const validarCPF = (strCPF: string) => {
  let Soma;
  let Resto;
  Soma = 0;
  if (strCPF == '00000000000') return false;

  for (let i = 1; i <= 9; i++) Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (11 - i);
  Resto = (Soma * 10) % 11;

  if (Resto == 10 || Resto == 11) Resto = 0;
  if (Resto != parseInt(strCPF.substring(9, 10))) return false;

  Soma = 0;
  for (let i = 1; i <= 10; i++) Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (12 - i);
  Resto = (Soma * 10) % 11;

  if (Resto == 10 || Resto == 11) Resto = 0;
  if (Resto != parseInt(strCPF.substring(10, 11))) return false;
  return true;
};

const validationSchema = Yup.object().shape({
  name: Yup.string().min(2, 'Nome precisa contar ao menos 2 caracteres').required('Nome é obrigatório'),
  surname: Yup.string().min(2, 'Nome precisa contar ao menos 2 caracteres').required('Nome é obrigatório'),
  email: Yup.string().email('Inserir um e-mail válido').required('E-mail é obrigatório'),
  cpf: Yup.number().required('CPF é obrigatório'),
  phone: Yup.number().required('Telefone é obrigatório'),
});

interface Party {
  name?: string;
  surname?: string;
  email?: string;
  cpf?: string;
  phone?: string;
}

const newParty: Party = { name: '', surname: '', email: '', cpf: '', phone: '' };

const DetailParty = () => {
  const { id } = useParams();
  const history = useHistory();
  const [state, dispatch] = useContext(Context);
  const [oper, setOper] = useState(id === 'new' ? 'create' : 'show');
  const [party, setParty] = useState<Party>(newParty);
  const [errorMsg, setErrorMsg] = useState('');

  useEffect(() => {
    const fetchParties = async () => {
      try {
        const response = await Api.get('parties', {
          headers: {
            Authorization: `Bearer ${state.user.token}`,
          },
        });

        dispatch({ type: 'SET_PARTIES', payload: { parties: response.data } });
        const partydb = response.data.find((p) => p._id === id);
        if (!partydb) {
          history.replace('/notfound');
        }
        setParty(partydb);
      } catch (err) {}
    };

    if (id === 'new') {
      return;
    }

    if (state.parties.length === 0) {
      fetchParties();
    } else {
      const party = state.parties.find((p) => p._id === id);
      if (!party) {
        history.replace('/notfound');
      }
      setParty(party);
    }
  }, []);

  const handleDelete = async () => {
    try {
      await Api.delete(`parties/${id}`, { headers: { Authorization: `Bearer ${state.user.token}` } });
      dispatch({ type: 'DELETE_PARTY', payload: { id } });
      history.push('/parties');
    } catch (err) {}
  };

  const handleSave = async (values, { setSubmitting, resetForm }) => {
    if (!validarCPF(values.cpf)) {
      setErrorMsg('CPF inválido');
      return;
    }

    if (values.phone.length < 10) {
      setErrorMsg('Telefone inválido. Considerar DDD');
      return;
    }

    if (oper === 'create') {
      try {
        const newParty = await Api.post('parties', values, { headers: { Authorization: `Bearer ${state.user.token}` } });
        dispatch({ type: 'ADD_PARTY', payload: { party: newParty } });
        history.push('/parties');
      } catch (err) {}
    } else if (oper === 'edit') {
      try {
        const updatedParty = await Api.put(`parties/${id}`, values, { headers: { Authorization: `Bearer ${state.user.token}` } });
        dispatch({ type: 'EDIT_PARTY', payload: { party: updatedParty } });
        history.push('/parties');
      } catch (err) {}
    }
  };

  if (!party) {
    return null;
  }

  return (
    <Container className="frame">
      <p>Detalhes da parte:</p>
      <Formik initialValues={party} validationSchema={validationSchema} onSubmit={handleSave} enableReinitialize>
        {({ values, errors, touched, handleChange, handleBlur, handleSubmit, isSubmitting, handleReset }) => (
          <Form noValidate onSubmit={handleSubmit}>
            <Form.Group controlId="formPartyName">
              <Form.Label>Nome:</Form.Label>
              <Form.Control
                plaintext={oper === 'show' ? true : false}
                readOnly={oper === 'show' ? true : false}
                type="text"
                placeholder="Nome"
                name="name"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.name}
                isInvalid={touched.name && errors.name}
              />
              {touched.name && errors.name ? <FormControl.Feedback type="invalid">{errors.name}</FormControl.Feedback> : null}
            </Form.Group>
            <Form.Group controlId="formPartySurname">
              <Form.Label>Sobrenome:</Form.Label>
              <Form.Control
                plaintext={oper === 'show' ? true : false}
                readOnly={oper === 'show' ? true : false}
                type="text"
                placeholder="Sobrenome"
                name="surname"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.surname}
                isInvalid={touched.surname && errors.surname}
              />
              {touched.surname && errors.surname ? <FormControl.Feedback type="invalid">{errors.surname}</FormControl.Feedback> : null}
            </Form.Group>
            <Form.Group controlId="formPartyEmail">
              <Form.Label>E-mail:</Form.Label>
              <Form.Control
                plaintext={oper === 'show' ? true : false}
                readOnly={oper === 'show' ? true : false}
                type="email"
                placeholder="E-mail"
                name="email"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.email}
                isInvalid={touched.email && errors.email}
              />
              {touched.email && errors.email ? <FormControl.Feedback type="invalid">{errors.email}</FormControl.Feedback> : null}
            </Form.Group>
            <Form.Group controlId="formPartyCPF">
              <Form.Label>CPF:</Form.Label>
              <Form.Control
                plaintext={oper === 'show' ? true : false}
                readOnly={oper === 'show' ? true : false}
                type="text"
                placeholder="CPF"
                name="cpf"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.cpf}
                isInvalid={touched.cpf && errors.cpf}
              />
              {touched.cpf && errors.cpf ? <FormControl.Feedback type="invalid">{errors.cpf}</FormControl.Feedback> : null}
            </Form.Group>
            <Form.Group controlId="formPartyPhone">
              <Form.Label>Telefone:</Form.Label>
              <Form.Control
                plaintext={oper === 'show' ? true : false}
                readOnly={oper === 'show' ? true : false}
                type="text"
                placeholder="Telefone"
                name="phone"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.phone}
                isInvalid={touched.phone && errors.phone}
              />
              {touched.phone && errors.phone ? <FormControl.Feedback type="invalid">{errors.phone}</FormControl.Feedback> : null}
            </Form.Group>
            {oper === 'show' && (
              <>
                <Button
                  variant="outline-primary"
                  onClick={() => {
                    setOper('edit');
                  }}
                >
                  Editar
                </Button>{' '}
                <Button variant="outline-danger" onClick={handleDelete}>
                  Deletar
                </Button>
              </>
            )}
            {(oper === 'create' || oper === 'edit') && (
              <>
                <Button variant="outline-primary" type="submit">
                  Salvar
                </Button>{' '}
                <Button
                  variant="outline-danger"
                  onClick={() => {
                    if (oper === 'create') {
                      history.push('/parties');
                    } else {
                      setOper('show');
                      handleReset();
                    }
                  }}
                >
                  Cancelar
                </Button>
              </>
            )}
          </Form>
        )}
      </Formik>
      <br />
      {errorMsg && <Alert variant="danger">{errorMsg}</Alert>}
    </Container>
  );
};

export default DetailParty;
