import React, { useContext, useEffect } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { Container, Table, Button } from 'react-bootstrap';
import Api from '../../services/api';

import { Context } from '../../Store';

const Parties = () => {
  const history = useHistory();
  const [state, dispatch] = useContext(Context);

  useEffect(() => {
    const fetchParties = async () => {
      try {
        const response = await Api.get('parties', {
          headers: {
            Authorization: `Bearer ${state.user.token}`,
          },
        });

        dispatch({ type: 'SET_PARTIES', payload: { parties: response.data } });
      } catch (err) {}
    };

    fetchParties();
  }, []);

  return (
    <Container className="frame">
      <p>Partes existentes:</p>
      <Table striped bordered hover>
        <thead>
          <tr className="text-center">
            <th>Nome</th>
            <th>Sobrenome</th>
            <th>E-mail</th>
            <th>CPF</th>
            <th>Telefone</th>
          </tr>
        </thead>
        <tbody>
          {state.parties.map((party) => (
            <tr key={party._id} className="text-center">
              <td>
                <Link to={`/parties/${party._id}`}>{party.name}</Link>
              </td>
              <td>{party.surname}</td>
              <td>{party.email}</td>
              <td>{party.cpf}</td>
              <td>{party.phone}</td>
            </tr>
          ))}
        </tbody>
      </Table>
      <Button variant="outline-primary" onClick={() => history.push('/parties/new')}>
        Nova parte
      </Button>
    </Container>
  );
};

export default Parties;
