import React, { useContext, useEffect } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { Container, Table, Button } from 'react-bootstrap';
import Api from '../../services/api';

import { Context } from '../../Store';

const Contracts = () => {
  const history = useHistory();
  const [state, dispatch] = useContext(Context);

  useEffect(() => {
    const fetchContracts = async () => {
      try {
        const response = await Api.get('contracts', {
          headers: {
            Authorization: `Bearer ${state.user.token}`,
          },
        });

        dispatch({ type: 'SET_CONTRACTS', payload: { contracts: response.data } });
      } catch (err) {}
    };

    fetchContracts();
  }, []);

  return (
    <Container className="frame">
      <p>Contratos existentes:</p>
      <Table striped bordered hover>
        <thead>
          <tr className="text-center">
            <th>Título</th>
            <th>Início</th>
            <th>Vencimento</th>
          </tr>
        </thead>
        <tbody>
          {state.contracts.map((contract) => (
            <tr key={contract._id} className="text-center">
              <td>
                <Link to={`/contracts/${contract._id}`}>{contract.title}</Link>
              </td>
              <td>{contract.begind}</td>
              <td>{contract.endd}</td>
            </tr>
          ))}
        </tbody>
      </Table>
      <Button variant="outline-primary" onClick={() => history.push('/contracts/new')}>
        Novo contrato
      </Button>
    </Container>
  );
};

export default Contracts;
