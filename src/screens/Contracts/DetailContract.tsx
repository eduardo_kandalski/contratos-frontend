import React, { useContext, useEffect, useState } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import { Container, Form, Button, FormControl, Alert } from 'react-bootstrap';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { IconContext } from 'react-icons';
import { FiDownload } from 'react-icons/fi';
import Api from '../../services/api';
import ContractPartiesAdd from '../../components/ContractPartiesAdd';
import ContractParties from '../../components/ContractParties';

import { Context } from '../../Store';
import bsCustomFileInput from 'bs-custom-file-input';

interface Contract {
  title?: string;
  begind?: string;
  endd?: string;
  filepath?: string;
  parties: string[];
}

const validationSchema = Yup.object().shape({
  title: Yup.string().min(2, 'Título precisa contar ao menos 2 caracteres').required('Título é obrigatório'),
  begind: Yup.string()
    .matches(/^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$/, 'Data inválida. Formato DD/MM/YYYY')
    .required('Início é obrigatório'),
  endd: Yup.string()
    .matches(/^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$/, 'Data inválida. Formato DD/MM/YYYY')
    .required('Fim é obrigatório'),
});

const newContract: Contract = {
  title: '',
  begind: '',
  endd: '',
  filepath: '',
  parties: [],
};

const DetailContract = () => {
  const { id } = useParams();
  const history = useHistory();
  const [state, dispatch] = useContext(Context);
  const [oper, setOper] = useState(id === 'new' ? 'create' : 'show');
  const [contract, setContract] = useState<Contract>(newContract);
  const [selectedFile, setSelectedFile] = useState(null);
  const [errorMsg, setErrorMsg] = useState('');

  useEffect(() => {
    const fetchContracts = async () => {
      try {
        const response = await Api.get('contracts', {
          headers: {
            Authorization: `Bearer ${state.user.token}`,
          },
        });

        dispatch({ type: 'SET_CONTRACTS', payload: { contracts: response.data } });
        const contractdb = response.data.find((c) => c._id === id);
        if (!contractdb) {
          history.replace('/notfound');
        }
        setContract(contractdb);
      } catch (err) {}
    };

    bsCustomFileInput.init();

    if (id === 'new') {
      return;
    }

    if (state.contracts.length === 0) {
      fetchContracts();
    } else {
      const contract = state.contracts.find((c) => c._id === id);
      if (!contract) {
        history.replace('/notfound');
      }
      setContract(contract);
    }
  }, []);

  useEffect(() => {
    const fetchParties = async () => {
      try {
        const response = await Api.get('parties', {
          headers: {
            Authorization: `Bearer ${state.user.token}`,
          },
        });

        dispatch({ type: 'SET_PARTIES', payload: { parties: response.data } });
      } catch (err) {}
    };

    fetchParties();
  }, []);

  const handleDelete = async () => {
    try {
      await Api.delete(`contracts/${id}`, { headers: { Authorization: `Bearer ${state.user.token}` } });
      dispatch({ type: 'DELETE_CONTRACT', payload: { id } });
      history.push('/contracts');
    } catch (err) {}
  };

  const handleSave = async (values, { setSubmitting, resetForm }) => {
    if (oper === 'create') {
      try {
        let fileUpload = undefined;
        if (selectedFile) {
          const fileData = new FormData();
          fileData.append('file', selectedFile);
          const uploadRes = await Api.post('contracts/upload', fileData, { headers: { Authorization: `Bearer ${state.user.token}` } });
          fileUpload = uploadRes.data;
        }

        const newContract = await Api.post(
          'contracts',
          {
            title: values.title,
            begind: values.begind,
            endd: values.endd,
            filepath: fileUpload ? fileUpload.filename : '',
            parties: contract.parties,
          },
          { headers: { Authorization: `Bearer ${state.user.token}` } }
        );
        dispatch({ type: 'ADD_CONTRACT', payload: { contract: newContract } });
        history.push('/contracts');
      } catch (err) {}
    } else if (oper === 'edit') {
      try {
        const updatedContract = await Api.put(
          `contracts/${id}`,
          {
            title: values.title,
            begind: values.begind,
            endd: values.endd,
            parties: contract.parties,
          },
          { headers: { Authorization: `Bearer ${state.user.token}` } }
        );
        dispatch({ type: 'EDIT_CONTRACT', payload: { contract: updatedContract } });
        history.push('/contracts');
      } catch (err) {}
    }
  };

  const handleDownload = async () => {
    try {
      const download = await Api.get(`download/${contract.filepath}`, {
        headers: { Authorization: `Bearer ${state.user.token}` },
        responseType: 'blob',
      });
      const url = window.URL.createObjectURL(new Blob([download.data]));
      const link = document.createElement('a');
      link.href = url;
      link.setAttribute('download', contract.filepath);
      document.body.appendChild(link);
      link.click();
    } catch (err) {}
  };

  const handlePartyAdded = (id: string, formState) => {
    const newContract = {
      ...contract,
      title: formState.title,
      begind: formState.begind,
      endd: formState.endd,
      parties: [...contract.parties],
    };
    newContract.parties.push(id);
    setContract(newContract);
  };

  const handlePartyRemoved = (id: string, formState) => {
    const newParties = contract.parties.filter((p) => p !== id);
    setContract({ ...contract, title: formState.title, begind: formState.begind, endd: formState.endd, parties: newParties });
  };

  if (!contract) {
    return null;
  }

  return (
    <Container className="frame">
      <p>Detalhes do contrato:</p>
      <Formik
        initialValues={{ title: contract.title, begind: contract.begind, endd: contract.endd }}
        validationSchema={validationSchema}
        onSubmit={handleSave}
        enableReinitialize
      >
        {({ values, errors, touched, handleChange, handleBlur, handleSubmit, isSubmitting, handleReset }) => (
          <Form noValidate onSubmit={handleSubmit}>
            <Form.Group controlId="formContractTitle">
              <Form.Label>Título:</Form.Label>
              <Form.Control
                plaintext={oper === 'show' ? true : false}
                readOnly={oper === 'show' ? true : false}
                type="text"
                placeholder="Título"
                name="title"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.title}
                isInvalid={touched.title && errors.title}
              />
              {touched.title && errors.title ? <FormControl.Feedback type="invalid">{errors.title}</FormControl.Feedback> : null}
            </Form.Group>
            <Form.Group controlId="formContractBegind">
              <Form.Label>Início:</Form.Label>
              <Form.Control
                plaintext={oper === 'show' ? true : false}
                readOnly={oper === 'show' ? true : false}
                type="text"
                name="begind"
                placeholder="DD/MM/AAAA"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.begind}
                isInvalid={touched.begind && errors.begind}
              />
              {touched.begind && errors.begind ? <FormControl.Feedback type="invalid">{errors.begind}</FormControl.Feedback> : null}
            </Form.Group>
            <Form.Group controlId="formContractEndd">
              <Form.Label>Fim:</Form.Label>
              <Form.Control
                plaintext={oper === 'show' ? true : false}
                readOnly={oper === 'show' ? true : false}
                type="text"
                name="endd"
                placeholder="DD/MM/AAAA"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.endd}
                isInvalid={touched.endd && errors.endd}
              />
              {touched.endd && errors.endd ? <FormControl.Feedback type="invalid">{errors.endd}</FormControl.Feedback> : null}
            </Form.Group>
            {oper === 'create' && (
              <Form.Group controlId="formContractFile">
                <Form.File
                  id="formFile"
                  label="Arquivo"
                  data-browse="Selecionar"
                  custom
                  onChange={(e) => setSelectedFile(e.target.files[0])}
                  accept="application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document, application/pdf"
                />
              </Form.Group>
            )}
            <ContractParties parties={contract.parties} removeParty={handlePartyRemoved} mode={oper} upperState={values} />
            {(oper === 'create' || oper === 'edit') && (
              <ContractPartiesAdd addedParties={contract.parties} addParty={handlePartyAdded} upperState={values} />
            )}
            {oper === 'show' && (
              <>
                {contract.filepath && (
                  <>
                    <p>Download do arquivo:</p>
                    <IconContext.Provider value={{ size: '2em', className: 'icon-cont' }}>
                      <div>
                        <FiDownload onClick={handleDownload} />
                      </div>
                    </IconContext.Provider>
                  </>
                )}
                <Button
                  variant="outline-primary"
                  onClick={() => {
                    setOper('edit');
                  }}
                >
                  Editar
                </Button>{' '}
                <Button variant="outline-danger" onClick={handleDelete}>
                  Deletar
                </Button>
              </>
            )}
            {(oper === 'create' || oper === 'edit') && (
              <>
                <Button variant="outline-primary" type="submit">
                  Salvar
                </Button>{' '}
                <Button
                  variant="outline-danger"
                  onClick={() => {
                    if (oper === 'create') {
                      history.push('/contracts');
                    } else if (oper === 'edit') {
                      setOper('show');
                      const db = state.contracts.find((c) => c._id === id);
                      setContract({ ...db });
                      handleReset();
                    }
                  }}
                >
                  Cancelar
                </Button>
              </>
            )}
          </Form>
        )}
      </Formik>
      <br />
      {errorMsg && <Alert variant="danger">{errorMsg}</Alert>}
    </Container>
  );
};

export default DetailContract;
